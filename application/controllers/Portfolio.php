<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Portfolio extends CI_Controller
	{
		var $gallery_path;
		public function __construct()
		{
			parent::__construct();
			$this->gallery_path = realpath(APPPATH . '../assets/image/portfolio/');
			$this->load->helper(array('form', 'url', 'html'));
		}

		public function add(){

			if ($_POST['btnAddPortfolio']) {
				$image = $this->input->post('userfile');

				$config['upload_path'] = './assets/image/portfolio/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 5000;
				$config['height'] = 250;
				$config['width'] = 200;

				$this->load->library('upload', $config);
				$this->image_lib->resize();
				$this->upload->initialize($config);

				if ($this->upload->do_upload()) {
					$data = array('upload_data' => $this->upload->data());
					$file_name = $data['upload_data']['file_name'];
					$image_name = $file_name;
				}

				$insert_portfolio = array(
					'id'		=> md5(microtime()),
					'title' => $this->input->post('title'), 
					'category' => $this->input->post('bidang'), 
					'detail' => $this->input->post('detail'), 
					'image' => $image_name,
					'date' => $this->input->post('date'),
					'client' => $this->input->post('client')
					);

				$res = $this->M_portfolio->addPortfolio('portfolio', $insert_portfolio);

				if ($res >= 1) {
					redirect('admin/portfolio');
				}else{
					echo('Insert Data Gagal');
				}
			}
		}

		public function delete($id)
		{
			$where = array('id' => $id);
			$res = $this->M_portfolio->deletePortfolio('portfolio', $where);
			redirect('admin/portfolio');
		}

		public function update($id)
		{
			$image = $this->input->post('userfile');

			$update_portfolio = array(
					'title' => $this->input->post('title'), 
					'category' => $this->input->post('category'), 
					'detail' => $this->input->post('detail'), 
					'date' => $this->input->post('date'),
					'client' => $this->input->post('client')
					);

			$res = $this->M_portfolio->updatePortfolio('portfolio', $update_portfolio, array('id' => $id));

			if ($res >= 1) {
				redirect('admin/portfolio');
			}else{
				echo('Update Data Gagal');
			}
		}

	}
 ?>